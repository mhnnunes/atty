
export function sendMessage(number, message, type){
        var number = number.replace(/[^\d]/g,"");
        if (type === 'sms'){
            var SmsAndroid = require('react-native-sms-android');
            SmsAndroid.sms(
                      number, // phone number to send sms to
                      message, // sms body
                      'sendDirect', // sendDirect or sendIndirect
                      (err, message) => {
                        if (err){
                          console.log("error");
                        } else {
                          console.log(message); // callback message
                        }
                      }
                    );
        }
        else{
            console.log("Not supported");
        }
    }