import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet} from 'react-native';

import UserAvatar from 'react-native-user-avatar';

const styles = StyleSheet.create({
  contactrow: {
    flex: 1,
    padding: 12,
    flexDirection: 'row',
    alignItems: 'center',
   },
  clickedrow: {
    backgroundColor: 'rgba(107, 38 , 171, 0.5)',
    flex: 1,
    padding: 12,
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    marginLeft: 12,
    fontSize: 18,
    fontWeight : 'bold'
  }
});


class ContactRow extends Component{

  constructor(props){
    super(props);
    this.state = {
      statusPressed : false,
      contact: this.props.contact
    };
  }

  changeStatusPressed(){
    if(this.state.statusPressed){
      this.props.observer.removeUncheckedContact(this.state.contact);
      this.setState({statusPressed: false});
    }else{
      this.props.observer.addCheckedContact(this.state.contact);
      this.setState({statusPressed: true});
    }
  }

  render(){
    var full_name = this.props.contact.givenName + (this.props.contact.familyName ? ' ' + this.props.contact.familyName : '');
    return(
      <TouchableOpacity
        key={this.props.contact.recordID}
        onPress={()=> {this.changeStatusPressed();} } >
        <View style={this.state.statusPressed ? styles.clickedrow : styles.contactrow} >
          <UserAvatar size="50" name={full_name} src={this.props.contact.thumbnailPath} />
          <Text style={styles.text}>{full_name}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

export default ContactRow;
