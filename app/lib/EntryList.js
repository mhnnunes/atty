import React, { Component } from 'react';
import {List, ListItem} from 'react-native-elements';
import {Text,  ListView} from 'react-native';
import realm from '../model/RealmConfig.js';

class EntryRow extends Component{

  constructor(props){
    super(props);
    this.state = {
      entry: this.props.entry,
      switchButton : this.props.entry.active
    };
  }

  toggleSwitch = () => {
    let entry = realm.objects('Entry').filtered('id = '+String(this.props.entry.id))[0];
    realm.write(() => {
        entry.active = !this.state.switchButton;
    });
    this.setState({switchButton: !this.state.switchButton});
    console.log('ENTRY', entry);
  }

  render(){
    let entry = realm.objects('Entry').filtered('id = '+String(this.props.entry.id))[0];
    return(
      <ListItem
          title={this.props.entry.contactname}
          subtitle={this.props.entry.address}
          switchButton={true}
          switchOnTintColor='#b15eff'
          switchThumbTintColor='#8a2be2'
          switched={entry.active}
          onSwitch={this.toggleSwitch}
          hideChevron
      />
    )
  }
}

const ds = new ListView.DataSource({
  rowHasChanged: (r1, r2) => r1 !== r2,
  sectionHeaderHasChanged: (h1, h2) => h1 !== h2,
});


class EntryList extends Component{
  constructor(props){
    super(props);

  }

  render(){
    if(this.props.entries.length === 0){
      return <Text></Text>
    }
    return (
      <List>
        <ListView
          dataSource={ds.cloneWithRows(this.props.entries)}
          renderRow={(rowData) => <EntryRow entry={rowData} /> }
        />
      </List>

    )
  }

}


export default EntryList;
