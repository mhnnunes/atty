import React, { Component } from 'react';
import {List} from 'react-native-elements';
import {
  Text,
  ListView} from 'react-native';

import ContactRow from '../lib/ContactRow.js'

const ds = new ListView.DataSource({
  rowHasChanged: (r1, r2) => r1 !== r2,
  sectionHeaderHasChanged: (h1, h2) => h1 !== h2,
});


class ContactList extends Component{

  constructor(props){
    super(props);
  }

  addCheckedContact(contact){
    this.props.observer.addCheckedContact(contact);
  }

  removeUncheckedContact(contact){
    this.props.observer.removeUncheckedContact(contact);
  }

  render(){

    if(this.props.contacts.length === 0){
      return <Text>No Contacts Loaded...</Text>
    }

    return (
      <List>
        <ListView
          dataSource={ds.cloneWithRows(this.props.contacts)}
          renderRow={(rowData) => <ContactRow contact={rowData} observer={this}/> }
        />
      </List>
    )
  }

}

export default ContactList;
