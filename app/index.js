import React, { Component } from 'react';
import { Root } from './config/router';

import BackgroundGeolocation from 'react-native-mauron85-background-geolocation';
import realm from './model/RealmConfig.js';
import { isPointInCircle } from 'geolib';
import {sendMessage} from './lib/Message'

import updateDb from './lib/UpdateEvent.js'

class App extends Component {
  constructor(props){
    super(props);
    this.eventUpdater = new updateDb();
  }

  componentDidMount() {
    let locations = realm.objects('Entry').filtered('active=true');
    console.log('ON BackgroundGeolocation: ', locations);

    BackgroundGeolocation.configure({
      desiredAccuracy: 10,
      stationaryRadius: 25,
      distanceFilter: 50,
      debug: false,
      locationProvider: BackgroundGeolocation.provider.ANDROID_DISTANCE_FILTER_PROVIDER,
      interval: 10000,
      fastestInterval: 5000,
      stopOnStillActivity: false,
      syncThreshold: 10,
      maxLocations: 10,
    });

    BackgroundGeolocation.on('location', (location) => {
      console.log('[DEBUG] BackgroundGeolocation location', location.latitude, location.longitude);
      let entries = realm.objects('Entry').filtered('active=true');
      console.log('ON BackgroundGeolocation: ', entries);

      var changed = [];

      entries.forEach((e) => {
        if(isPointInCircle(
            {latitude: location.latitude, longitude: location.longitude},
            {latitude: e.location.latitude, longitude: e.location.longitude},
            50
        )){
        console.log('numero:  ', e.contactnumber, 'mensagem: ', e.message);
        sendMessage(e.contactnumber, e.message,'sms');

        changed.push(e.id);
        }
      });

      if (changed.length != 0){
        changed.forEach((c) =>{
          let entry = realm.objects('Entry').filtered('id = '+String(c))[0];
          realm.write(() => { entry.active = false;});
        });
        this.eventUpdater.updateDb();
      }

    });

    BackgroundGeolocation.on('stationary', (stationaryLocation) => {
       console.log('[DEBUG] BackgroundGeolocation stationary location', stationaryLocation.latitude, stationaryLocation.longitude);
    });


    BackgroundGeolocation.on('error', (error) => {
      console.log('[ERROR] BackgroundGeolocation error:', error);
    });

    BackgroundGeolocation.start(() => {
      console.log('[DEBUG] BackgroundGeolocation started successfully');
    });
  }


  render() {
    return <Root screenProps={this.eventUpdater}/>;
  }
}

export default App;
