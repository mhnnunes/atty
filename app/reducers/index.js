import { ADD_CONTACT, REMOVE_CONTACT, ADD_LOCATION, ADD_MESSAGE } from '../constants/ActionTypes.js'

const initialState = {
  contacts: [],
  location: {},
  message: ''
}

export default function attyApp(state = initialState, action) {

  switch (action.type) {
    case ADD_MESSAGE:
      return Object.assign({}, state, {
       message: action.message
      })

      break;
    case ADD_CONTACT:
      return Object.assign({}, state, {
        contacts: [
          ...state.contacts,
          {
            id: action.contact.recordID,
            contact: action.contact
          }
        ]
      })
    case REMOVE_CONTACT:
      return Object.assign({}, state, {
        contacts: state.contacts.filter(contact =>
                  contact.id !== action.contact.recordID)
      })
    case ADD_LOCATION:
      return Object.assign({}, state, {
        location: action.location
      })

    default:
      return state;
  }
  return state
}
