import { createStore } from 'redux';

import attyApp from './index.js';

export default function configureStore() {
  let store = createStore(attyApp)
  return store
}
