/*
 * action types
 */
import { ADD_CONTACT, REMOVE_CONTACT, ADD_LOCATION, ADD_MESSAGE } from '../constants/ActionTypes.js'

/*
 * action creators
 */

 export function addContact(contact){
   return {
     type: ADD_CONTACT,
     contact: contact
   }
 }

 export function removeContact(contact){
   return {
     type: REMOVE_CONTACT,
     contact: contact
   }
 }

 export function addLocation(location){
   return {
     type: ADD_LOCATION,
     location: location
   }
 }

 export function addMessage(message){
   return {
     type: ADD_MESSAGE,
     message: message
   }
 }

export const boundAddContact = (contact, store) => store.dispatch(addContact(contact));
export const boundRemoveContact = (contact, store) => store.dispatch(removeContact(contact));
export const boundAddLocation = (location, store) => store.dispatch(addLocation(location));
export const boundAddMessage = (message, store) => store.dispatch(addMessage(message));
