import Realm from 'realm';

class PhoneNumber extends Realm.Object{}
PhoneNumber.schema = {
  name: 'PhoneNumber',
  properties: {
    label: 'string',
    number: 'string',
  },
}
//
class Contact extends Realm.Object {}
Contact.schema = {
  name: 'Contact',
  properties: {
    phoneNumbers: {type: 'list', objectType: 'PhoneNumber'},
    recordID: 'string',
    givenName: {type: 'string', optional: true},
  },
}
//
class Location extends Realm.Object {}
Location.schema = {
  name: 'Location',
  properties: {
    latitude: 'double',
    longitude: 'double',
  },
}

class Entry extends Realm.Object {}
Entry.schema = {
  name: 'Entry',
  primaryKey: 'id',
  properties: {
    location: {type: 'Location', optional: true},
    id: 'int',
    contactname: 'string',
    contactnumber: 'string',
    address: 'string',
    message: 'string',
    active: 'bool',
  },
}

export default new Realm({schema: [PhoneNumber, Contact, Location, Entry]});
