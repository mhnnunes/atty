import realm from './RealmConfig.js';
import configureStore from '../reducers/configureStore.js'
function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

export default class DBhandler{
  checkValidEntry(entry){
      var valid = false;
      if(entry.contacts.length === 0 || entry.contacts.length > 1){
        console.log("0 OR MORE THAN ONE CONTACT SELECTED");
        alert("Please select exactly one contact from the contacts list.");
      }else if(isEmpty(entry.location)){
        console.log("NO LOCATION SAVED");
        alert("Please select a location in the map.");
      }else if(entry.message === ''){
        console.log("NO MESSAGE SAVED");
        alert("Please type in a message");
      }else{
        valid = true;
      }

      return valid;
  }
  saveEntry(entry){
    if(this.checkValidEntry(entry)){
      var contact_name = '';

      let entries = realm.objects('Entry');
      if(entry.contacts[0].contact.givenName){
        contact_name += entry.contacts[0].contact.givenName;
      }

      if(entry.contacts[0].contact.familyName){
        if(entry.contacts[0].contact.givenName){
          contact_name += ' ';
        }
        contact_name += entry.contacts[0].contact.familyName;
      }
      realm.write(() => {
        let e = realm.create('Entry', {
          id: entries.length,
          contactname: contact_name,
          contactnumber: entry.contacts[0].contact.phoneNumbers[0].number,
          location: {
            latitude: entry.location.latitude,
            longitude: entry.location.longitude,
          },
          address: entry.location.address,
          message: entry.message,
          active: true,
        }, true);
      })
      alert("Success! Saved a new entry.");

    }
  }
}
