import React from 'react';
import { TabNavigator, StackNavigator } from 'react-navigation';
import { Icon } from 'react-native-elements';

import HomeScreen from '../screens/HomeScreen';
import MapScreen from '../screens/MapScreen';
import MakeEntryScreen from '../screens/MakeEntryScreen';
import ContactsScreen from '../screens/ContactsScreen';
import MessageScreen from '../screens/MessageScreen';



const color='#8a2be2'
export const Tab = TabNavigator({
  ContactsScreen: {
    screen: ContactsScreen,
    navigationOptions: {
      tabBarLabel: 'Contacts',
      title: 'Contacts',
      tabBarIcon: ({ tintColor }) => <Icon name="contacts" size={35} color={tintColor} />
    },
  },
  MapScreen: {
    screen: MapScreen,

    navigationOptions: {
      tabBarLabel: 'Map',
      title: 'Map',
      tabBarIcon: ({ tintColor }) => <Icon name="map" size={35} color={tintColor} />,
    },
  },
  MessageScreen: {
    screen: MessageScreen,
    navigationOptions: {
      tabBarLabel: 'Message',
      title: 'Message',
      tabBarIcon: ({ tintColor }) => <Icon name="message" size={35} color={tintColor} />,
      header :{
        title : "Type a message",
      }
    },
  },
},{
  tabBarOptions : {
    style : {
      backgroundColor:color,
    }
  }
});

export const Root = StackNavigator({
  Home: { 
      screen: HomeScreen,
      navigationOptions: {
        title: 'Atty',
      }
    },
  MakeEntryScreen: {
      screen: MakeEntryScreen,
      navigationOptions: {
        headerLeft : null,
        title: 'Make a new entry',
      }
    },
},{
    headerMode : 'screen',
    navigationOptions : {
      headerStyle : { backgroundColor : color, elevation : null},
      headerTitleStyle: {color: 'white', fontFamily: 'Helvetica'}
    }
});
