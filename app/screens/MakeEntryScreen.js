
import React, { Component } from 'react';
import { View, StyleSheet} from 'react-native';

import {Icon} from 'react-native-elements';
import { Tab } from '../config/router';
import configureStore from '../reducers/configureStore.js';
import DBhandler  from '../model/DBhandler.js';

const store = configureStore()

const styles = StyleSheet.create({
  acceptButton: {
   position: 'absolute',
   right: 0,
   bottom: 0,
 }
});

class MessageScreen extends Component{

  constructor(props){
    super(props);
    this.db = new DBhandler();
  }

  _save(navigate){
    let state = store.getState();
    this.db.saveEntry(state);
    store = configureStore();
    this.props.navigation.state.params.event.updateDb();
    navigate.goBack(null);

  }
  render(){
    const { navigate } = this.props.navigation;
    return(
        <View style={{ flex: 1 }}>
          <Tab screenProps={store} />
          <View style={styles.acceptButton} >
            <Icon
                reverse
                name='done'
                type='material'
                color='#8a2be2'
                onPress={() => {
                  this._save(this.props.navigation)
                }}
            />
          </View>
        </View>
      )
  }
}

export default MessageScreen;
