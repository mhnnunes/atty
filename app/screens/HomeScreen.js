import React, { Component } from 'react';
import { View } from 'react-native';

import { Icon } from 'react-native-elements';
import EntryList from '../lib/EntryList.js';
import realm from '../model/RealmConfig.js';


class HomeScreen extends React.Component {

  constructor(props){
    super(props);

    this.eventUpdater = this.props.screenProps;

    this.state={
      update:false
    }
  }

  componentWillMount(){
    this.eventUpdater.addListener('updateDb', () =>{
      this.handle_update();
    });
  }

  handle_update = () => { 
    this.setState({update:!this.state.update});
  }

  render() {
    const { navigate } = this.props.navigation;
    let entries = realm.objects('Entry');
    return(
      <View style={{flex: 1}}>
        <View style={{flex: 6}}>
          <EntryList entries={entries} />
        </View>
        <View style={{flex: 1, alignItems: 'center', flexDirection: 'row'}}>
          <View style={{flex: 1, alignItems: 'center'}}>
            <Icon reverse
              name = 'person-add'
              type = 'material'
              color = '#8a2be2'
              onPress={() =>
                  navigate('MakeEntryScreen', {event:this.eventUpdater})
              }
            />
          </View>
        </View>
      </View>
    );
  }
}

export default HomeScreen;
