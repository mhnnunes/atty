import MapView from 'react-native-maps';
import Geocoder from 'react-native-geocoder';

import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  ToastAndroid,
  Dimensions,
  TextInput
} from 'react-native';
import {boundAddLocation} from '../reducers/actions.js';
import {Icon} from 'react-native-elements';

const styles = StyleSheet.create({
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: -1
  },
  searchInput: {
   fontSize: 18,
   backgroundColor: 'white',
   paddingLeft: 20,
   height: 50,
   zIndex:10
 },
 acceptButton: {
   position: 'absolute',
   right: 0,
   bottom: 0,
 }
});

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.00922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const FILTER_TEXT = 'Search for an address..';
let id = 0;

class MapScreen extends Component {

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        console.log('hellow');
        var long = position.coords.longitude;
        this.setState({currentRegion: {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        }});
      },
      (error) => alert(JSON.stringify(error)),
      {enableHighAccuracy: false, timeout: 20000}
    );
  }

  constructor() {
    super();

    this.state = {
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      markers: [],
    };
  }

  onMapPress(e) {
    console.log("oin vinhadoww", this.state)
    var mycoordinate = e.nativeEvent.coordinate
    this.setState({
      currentRegion :{
        latitude : mycoordinate.latitude,
        longitude : mycoordinate.longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      markers: [
        {
          coordinate: mycoordinate,
          key: id,
        },
      ],
    });

    var coord = {
      lat: mycoordinate.latitude,
      lng: mycoordinate.longitude,
    };
    console.log(coord)
    Geocoder.geocodePosition(coord).then(res => {
      var position = res[0];
      console.log(position.formattedAddress);
      // this.props.screenProps.dispatch({
      //   type: ADD_LOCATION,
      //   location: {
      //     latitude: mycoordinate.latitude,
      //     longitude: mycoordinate.longitude,
      //     address: position.formattedAddress
      //   }
      // })
      let location = {
        latitude: mycoordinate.latitude,
        longitude: mycoordinate.longitude,
        address: position.formattedAddress
      };
      boundAddLocation(location, this.props.screenProps);
    })
    .catch(err => console.log(err))

  }

  updateMap(address) {

    if(address){
        Geocoder.geocodeAddress(address).then(res => {
          var position = res[0].position

          this.setState({currentRegion: {
            latitude: position.lat,
            longitude: position.lng,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }});
        })
        .catch(err => console.log(err))
    }
  }


_changeText = (v) => {
  this.setState({v});
}

_onSubmit = () => {
  console.log(this.state.text);
  this.updateMap(this.state.text);
  this.textInputRef.clear();
  this.setState({text : ''});
}

  render() {
    return (
      <View style={{flex: 1}}>
        <TextInput
          style={styles.searchInput}
          autoCapitalize="none"
          autoCorrect={false}
          clearButtonMode="always"
          returnKeyType="search"
          ref={ref => this.textInputRef = ref}
          value={this.state.text}
          onChangeText={(text) => this.setState({text})}
          onSubmitEditing={this._onSubmit}
          placeholder={FILTER_TEXT}
          testID="explorer_search"
        />
        <MapView.Animated
          style={ styles.map }
          region = {this.state.currentRegion}
          showUserLocation={true}
          onPress={(e) => this.onMapPress(e)}
        >
          {this.state.markers.map(marker => (
            <MapView.Marker
              key={marker.key}
              coordinate={marker.coordinate}
              pinColor={marker.color}
            />
          ))}
        </MapView.Animated>
      </View>
    );
  }
}

export default MapScreen;
