
import React, { Component } from 'react';
import {View, StyleSheet, TextInput } from 'react-native';

import { boundAddMessage } from '../reducers/actions.js'

const styles = StyleSheet.create({
  text: {
   fontSize: 18,
   backgroundColor: 'white',
   paddingLeft: 20,
   height: 100,
   borderColor: "#000",
   borderWidth: 1,
   margin: 30,
   textAlignVertical : 'top'
 },
});

class MakeEntryScreen extends Component{

  constructor(props){
    super(props);
    this.state = {text: ""}
  }

  render(){
    return(
        <View style={{flex: 3, backgroundColor: 'white'}}>
          <View>
            <TextInput
              style={styles.text}
              autoCapitalize="none"
              autoCorrect={false}
              clearButtonMode="always"
              multiline={true}
              onChangeText={(text) => {
                this.setState({text});
              }}
              onEndEditing={(text)=> {
                boundAddMessage(this.state.text, this.props.screenProps)
              }}
              value={this.state.text}
              placeholder={"Please type in a message"}
              testID="explorer_search"
            />

          </View>
        </View>
      );
  }
}

export default MakeEntryScreen;
