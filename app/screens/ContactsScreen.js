import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { _ } from 'underscore';
import Contacts from 'react-native-contacts';
import ContactList from '../lib/ContactList.js';
import { boundAddContact, boundRemoveContact } from '../reducers/actions.js';
class ContactsScreen extends Component {
    selectedContacts = {};
    constructor(props){
      super(props);
      this.state = {contacts: []};
      this._loadContacts();
    }

    _loadContacts(){
      Contacts.getAll((err, contacts) => {
        if(err === 'denied'){
          console.log("On ContactsScreen, got error: denied! (when loading contacts)");
        } else {
          //Sort contacts by name
          var newContacts = _.sortBy(contacts, function(a){ return a.givenName; });
          this.setState({contacts: newContacts});
        }
      });
    }

    addCheckedContact(contact){
      boundAddContact(contact, this.props.screenProps);
    }

    removeUncheckedContact(contact){
      boundRemoveContact(contact, this.props.screenProps);
    }
    render() {
      if(this.state.contacts.length === 0){
        return (<Text>No Contacts Loaded...</Text>);
      }

      return (
        <View style={{flex: 1, backgroundColor: 'white'}}>
          <ContactList contacts={this.state.contacts} observer={this}/>
        </View>
      );
    }
}

export default ContactsScreen;
