Atty - Are they there yet?
==========================================

### Description  

Atty (Are they there yet?) is an application for Android devices that notifies your loved ones when you arrive at places. Before you head out, you just pick a contact, pick a place and type a message and this notification will be saved. The app will then monitor your location in the background, and automatically send an SMS to the contact when you arrive at your destination.

### Usage

This is a react-native project. In order to run the code, please follow these steps:

1. Get the source code:  
    `$ git clone https://mhnnunes@bitbucket.org/mhnnunes/atty.git`  
2. Start react native:  
    `react-native start`  
3. Run the program:  
    `react-native run-android`  

### Dependencies
This software uses the following libraries: 

* react-native-maps

    Installation instructions can be found [here][react-native-maps installation instructions]
 
* react-native-contacts 

    Installation instructions can be found [here][react-native-contacts installation instructions]
 
* underscore 

     Installation instructions can be found [here][underscore installation instructions]
 
* react-native-elements

    Installation instructions can be found [here][react-native-elements installation instructions]

* react-native-geocoder

    Installation instructions can be found [here][react-native-geocoder installation instructions]

* react-native-user-avatar

    Installation instructions can be found [here][react-native-user-avatar installation instructions]

* geolib

    Installation instructions can be found [here][geolib installation instructions]

* react-native-background-geolocation

    Installation instructions can be found [here][react-native-background-geolocation installation instructions]

* react-native-sms-android

    Installation instructions can be found [here][react-native-sms-android installation instructions]
 

[react-native-maps installation instructions]: https://github.com/airbnb/react-native-maps/blob/master/docs/installation.md

[react-native-contacts installation instructions]: https://github.com/rt2zz/react-native-contacts

[underscore installation instructions]: http://underscorejs.org/

[react-native-elements installation instructions]: https://github.com/react-native-training/react-native-elements/blob/master/default_installation.md

[react-native-geocoder installation instructions]: https://github.com/devfd/react-native-geocoder

[react-native-user-avatar installation instructions]: https://github.com/avishayil/react-native-user-avatar

[geolib installation instructions]: https://github.com/manuelbieh/Geolib

[react-native-background-geolocation installation instructions]: https://github.com/mauron85/react-native-background-geolocation

[react-native-sms-android installation instructions]: https://github.com/rhaker/react-native-sms-android