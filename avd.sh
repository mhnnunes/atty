emulator -list-avds | cat -n
printf "Select AVD: "
read index
avd=$(emulator -list-avds | sed "${index}q;d")
echo "Selected $avd"
cd ~/Android/Sdk/tools
emulator -netdelay none -netspeed full -avd $avd
